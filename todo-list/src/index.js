//import { saludar } from './js/componentes';
import './styles.css';
import './css/componentes.css';
import { Todo, TodoList } from './classes';
import { crearTodoHtml } from './js/componentes';

export const todoList = new TodoList();

todoList.todos.forEach( crearTodoHtml );

console.log( 'todos', todoList.todos );


//const newTodo = new Todo('Aprender JavaScript');
//todoList.nuevoTodo( newTodo );
//todoList.todos[0].imprimirClase();
//newTodo.imprimirClase();

//const tarea = new Todo('Aprender JavaScript');
//todoList.nuevoTodo( tarea );

//tarea.completado = true;
//console.log( todoList );
//crearTodoHtml( tarea );

//local storage siempre debe ser un string
//localStorage.setItem('mi-key', 'ABV');
//sessionStorage.setItem('mi-key', 'ASD');

//timeout para borrar localStorage
/*
setTimeout( ()=>{

    localStorage.removeItem('mi-key');

}, 1500 );
*/