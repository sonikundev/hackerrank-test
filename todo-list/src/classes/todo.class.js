
export class Todo {

    static fromJson( { id, tarea, completado, creado} ){

        const tempTodo = new Todo( tarea );

        tempTodo.id         = id;
        tempTodo.completado = completado;
        tempTodo.creado     = creado;

        return tempTodo;
    }

    constructor( tarea ) {

        this.tarea = tarea;

        this.id         = new Date().getTime(); // 1283512312
        this.completado = false;
        this.creado     = new Date();

    }

    //Los metodos no se guardan en el local storage
    imprimirClase() {
        console.log(`${ this.tarea } - ${ this.id }`);
    }

}
