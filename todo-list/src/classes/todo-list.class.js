import { Todo } from './todo.class';

export class TodoList {

   constructor() {

     //this.todos = [];
     this.cargarLocalStorage();

   }

   nuevoTodo( todo ){
     this.todos.push( todo );
     this.guardarLocalStorage();//Aqui se invoca para que se guarde
   }

   eliminarTodo( id ){

    //filter crea un nuevo array con todos los elementos que cumplan la condicion 
    this.todos = this.todos.filter( todo => todo.id !== id ); 
    //retorna un arreglo excluyendo todo lo que coincida con el ID
    this.guardarLocalStorage();
    //this.todos = ( localStorage.removeItem( todo => todo.id !== id  ));
   }

   //Funcion para el radio button
   marcarCompletado( id ){

        for( const todo of this.todos ){ //el for es para ver cada uno de los elementos

            if( todo.id  == id ){ //si el id es igual al de la funcion marcarCompletado(id)

                //si todo.compleatdo es true, la negacion de !todo.completado es falso
                todo.completado = !todo.completado; 
                this.guardarLocalStorage();
                break;//para salir del ciclo
            }

        }

   }
   //Funcion para el radio button

   eliminarCompletados() {

    //que nos regrese todos los que no estan completados
    this.todos = this.todos.filter( todo => !todo.completado );
    this.guardarLocalStorage();
    //this.todos = ( localStorage.removeItem('todo') );
   }

   //Aqui
   guardarLocalStorage(){

        //convertir la variable a string con JSON
        localStorage.setItem('todo', JSON.stringify( this.todos ) );

   }
   //Aqui

   cargarLocalStorage(){
   // operador ternario
   this.todos = ( localStorage.getItem('todo') ) 
              ? JSON.parse(localStorage.getItem('todo'))//Si esto existe 
              : [];// si no regresa esto

   //map nos itera todos los elementos de una arreglo           
   //this.todos = this.todos.map( obj => Todo.fromJson( obj ) );//forma alterna
   this.todos = this.todos.map( Todo.fromJson );

   }
}             