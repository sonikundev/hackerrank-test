import { Todo } from '../classes'

import { todoList } from '../index';

// Referencias en el HTML
const divTodoList = document.querySelector('.todo-list');// aqui estan los elementos que vamos creando
const txtInput    = document.querySelector('.new-todo');
const btnBorrar   = document.querySelector('.clear-completed');


export const crearTodoHtml = ( todo ) => {

    //obtenemos el id de data-id="${ todo.id }"
    const htmlTodo = `
    <li class="${ (todo.completado) ? 'completed' : '' }" data-id="${ todo.id }">
        <div class="view">
            <input class="toggle" type="checkbox" ${ (todo.completado) ? 'checked' : '' }>
            <label>${ todo.tarea }</label>
            <button class="destroy"></button>
        </div>
        <input class="edit" value="Create a TodoMVC template">
    </li>`;

    const div = document.createElement('div');
    div.innerHTML = htmlTodo;

    divTodoList.append( div.firstElementChild );

    return div.firstElementChild;

}

//Eventos keycode para cachar el evento de enter
txtInput.addEventListener('keyup', (event) => {

    if ( event.keyCode === 13 && txtInput.value.length > 0 ) {

        console.log(txtInput.value);
        const nuevoTodo = new Todo( txtInput.value );
        todoList.nuevoTodo( nuevoTodo );

        //console.log(todoList);
        crearTodoHtml( nuevoTodo );
        txtInput.value = '';
    }

});

//cachar el evento del click del radio button
divTodoList.addEventListener('click', (event) =>{

    //console.log('click');
    //console.log(event.target.localName);//podemos identificar cada parte donde se hizo click
    //target nos dice donde estamos haciendo click
    //localname nos dice en que parte del li hicimos click

    const nombreElemento = event.target.localName; //referenciar input, label, button
    const todoElemento   = event.target.parentElement.parentElement;//nos da la referencia completa al li al que damos click
    const todoId         = todoElemento.getAttribute('data-id');//Recupera el id del ul
    //.getAttribute nos obtiene el dato que queremos, en este caso el id
    if( nombreElemento.includes('input') ){//valida si se hizo click en el check
        todoList.marcarCompletado( todoId );//marca completado todo con su ID
        todoElemento.classList.toggle('completed');
        //classList referencias las clases y toggle las cambia
    } else if( nombreElemento.includes('button') ){ // hay que borrar el todo
        todoList.eliminarTodo( todoId );
        divTodoList.removeChild( todoElemento );
    }
});
//cachar el evento del click del radio button


btnBorrar.addEventListener('click', () => {

    todoList.eliminarCompletados();

    //se resta .length-1 porque los arreglos empiezan en 1
    for( let i = divTodoList.children.length-1; i >= 0; i-- ){

        const elemento = divTodoList.children[i];

        //con esto puedo evaluar si contiene la clase
        if( elemento.classList.contains('completed') ){
            divTodoList.removeChild(elemento);   
        }


    }

});